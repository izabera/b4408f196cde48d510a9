#!/bin/awk -f
BEGIN {
  header = "\
_display () { echo \"$ $1\"; eval \"$1\"; }\n\
_needs () {\n\
  for i; do\n\
    command -v \"$i\" > /dev/null || break\n\
  done\n\
}"
}

/^# start$/ { start = 1 ; print header }
/^# stop$/ { start = 0 }
/^# restart$/ { start = 1 }
! start { print ; next }

/^# needs/ {
  needs = substr ($0, length("# needs") + 1)
}

/^[^#]/ {
  printf ("_needs%s && _display \"%s\"\n", needs, $0 )
}

/^#/ { print }
